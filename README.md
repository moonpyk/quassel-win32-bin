# Quassel IRC Win32 Binaries #

This is a **unofficial and download only** repository containing up-to-date Win32 binaries for the [Quassel IRC Client](http://www.quassel-irc.org/).

I'm compiling them using Visual Studio 2010 and official Qt 4.x binary releases.

The source code I used comes from the official git repository with no modification.

I'm only compiling **quasselclient**, no quasselcore neither quassel mono for now 

Why just quasselclient ? Because QCA and SSL toolchain are a bit complicated to get working with VS2010 and I'm personnaly using a quasselcore running on a Gentoo Linux box.

**Those are unofficial and unsupported binaries**, don't report bugs upstream if you are getting problems with them. I'm using them daily from my own needs with no problem though.

I'm publishing those binaries to help people in the same situation I am (using an always up-to-date remote quasselcore and just needing a fresh working quasselclient).

Files are compressed using [7zip](http://www.7-zip.org/) and are located inside the "[Downloads](https://bitbucket.org/moonpyk/quassel-win32-bin/downloads)" tab.

Regards. 

